using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class Player : MonoBehaviour
{
    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public Camera cam;

    Vector2 movement;

    Vector2 mousePos;


    private void Start()
    {
    }

    private void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

    mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    

    }

    private void FixedUpdate()
    {
       rb.MovePosition(rb.position + movement *moveSpeed *Time.deltaTime);       
       Vector2 lookDir = mousePos - rb.position;   

       float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg-90f;
        rb.rotation = angle;
    }

}
