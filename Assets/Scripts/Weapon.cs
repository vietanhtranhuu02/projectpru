using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public GameObject bullet;
    public Transform firePos;
    public GameObject muzzle;
    public GameObject fireEffect;

    public float TimeBtwFire = 0.2f; /*sung ban nhanh hay cham */
    public float bulletForce;
    private float timeBtwFire;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RotaGun();
        timeBtwFire -= Time.deltaTime;

        if(Input.GetMouseButton(0) && timeBtwFire < 0)
        {
            FireBullet();
        }
    }

    private void RotaGun()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 lookDir = mousePos - transform.position;
        float a = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(0, 0, a);
        transform.rotation = rotation;

        if (transform.eulerAngles.z > 90 && transform.eulerAngles.z < 270)
        {
            transform.localScale = new Vector3(1, -1, 0);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 0);
        }
    }

    void FireBullet()
    {
        timeBtwFire = TimeBtwFire;
        GameObject bulletTmp = Instantiate(bullet, firePos.position, Quaternion.identity);

        Instantiate(muzzle, firePos.position, transform.rotation, transform);
        Instantiate(fireEffect, firePos.position, transform.rotation, transform);
        
        Rigidbody2D rb = bulletTmp.GetComponent<Rigidbody2D>();
        rb.AddForce(transform.right * bulletForce, ForceMode2D.Impulse);
    }
}
